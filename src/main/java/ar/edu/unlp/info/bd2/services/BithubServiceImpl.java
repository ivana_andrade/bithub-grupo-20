package ar.edu.unlp.info.bd2.services;

import ar.edu.unlp.info.bd2.models.*;
import ar.edu.unlp.info.bd2.repositories.BithubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class BithubServiceImpl implements BithubService {

    @Autowired
    BithubRepository repository;


    public BithubServiceImpl(BithubRepository repository ) {
        this.repository=repository;
    }

    @Override
    @Transactional
    public User createUser(String email, String name) {
        User newUser=new User(email, name);
        repository.createUser(newUser);
        return newUser;
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return repository.getUserByEmail(email);
    }

    @Override
    @Transactional
    public Branch createBranch(String name) {
        Branch newBranch= new Branch(name);
        repository.createBranch(newBranch);
        return newBranch;
    }

    @Override
    @Transactional
    public Commit createCommit(String description, String hash, User author, List<File> files, Branch branch) {
        Commit newCommit = new Commit(description, hash, author, files, branch);
        repository.createCommit(newCommit);
        for (File file : files) {
            file.setCommit(newCommit);
        }
        branch.addCommit(newCommit);
        author.addCommit(newCommit);
        return newCommit;
    }

    @Override
    @Transactional
    public Tag createTagForCommit(String commitHash, String name) throws BithubException {
        Optional<Commit> commitOptional= this.getCommitByHash(commitHash);
        if(commitOptional.isPresent()) {
            Commit commit = commitOptional.get();
            Tag newTag = new Tag(commitHash, name, commit);
            repository.createTag(newTag);
            commit.setTag(newTag);
            return newTag;
        }else{
            throw new BithubException("The commit don't exist.");
        }
    }

    @Override
    public Optional<Commit> getCommitByHash(String commitHash) {
        return repository.getCommitByHash(commitHash);
    }

    @Override
    @Transactional
    public File createFile(String content, String name) {
        File newFile= new File(content, name);
        repository.createFile(newFile);
        return newFile;
    }

    @Override
    public Optional<Tag> getTagByName(String tagName) {
        return repository.getTagByName(tagName);
    }

    @Override
    @Transactional
    public Review createReview(Branch branch, User user) {
        Review newReview = new Review(branch, user);
        repository.createReview(newReview);
        return newReview;
    }

    @Override
    @Transactional
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException {
        if(file.getCommit().getBranch().equals(review.getBranch())) {
            FileReview newFileReview = new FileReview(review, file, lineNumber, comment);
            repository.createFileReview(newFileReview);
            review.addReview(newFileReview);
            file.addReview(newFileReview);
            return newFileReview;

        }else {
            throw new BithubException("The review's branch must be equals to file's branch");
        }
    }

    @Override
    public Optional<Review> getReviewById(long id) {
        return repository.getReviewById(id);
    }

    @Override
    public List<Commit> getAllCommitsForUser(long userId) {
        User u= repository.getUserForId(userId);
        return u.getCommits();
    }

    @Override
    public Map<Long, Long> getCommitCountByUser() {
        List<Long> ids= repository.getIdsForUsers(); //Obtengo todos los ids de los usuarios.
        Map<Long, Long> map= new HashMap<>();
        for (Long id: ids){
            List commits = repository.getCountCommitsForUserId(id); //Por cada id, traigo la cantidad de commits que realizó.
            Long count= (Long)commits.get(0);
            map.put(id,count);
        }
        return map;
    }

    @Override
    public List<User> getUsersThatCommittedInBranch(String branchName) throws BithubException {
        Optional<Branch> branchOpt= this.getBranchByName(branchName);
        if(branchOpt.isPresent()){
            Branch b = branchOpt.get();
            return(repository.getUsersThatCommittedInBranch(b));
        }else{
            throw new BithubException("The branch don't exist.");
        }
    }

    @Override
    public Optional<Branch> getBranchByName(String branchName) {
        return repository.getBranchByName(branchName);
    }

}
